//
// Created by jawwa on 5/6/2020.
//

#ifndef PROJECT_STACK_H
#define PROJECT_STACK_H
#include "point.h"

struct stack;
struct stack* create(int max);
int isFull(struct stack* s);
int isEmpty(struct stack* s);
void push(struct stack* s, struct Point element);
struct Point pop(struct stack* s);
struct Point peek(struct stack* s);
#endif //PROJECT_STACK_H

