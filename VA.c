#include <stdio.h>
#include <stdlib.h>
#include "point.h"
#include "stack.h"

#define MAX_SIZE 20
#define MAX_ENERGY 100
enum itc{
    noitch = 0,
    rightitch = 1,
    leftitch = 2,
    forwarditch = 3,
    backitch = 4,
}itc;

struct VA
{
    enum itc itch;
  //  unsigned capacity;
    int row;
    int col;
    int energy;
    char ** m;
};
  //for itch 0 = no itch
  // 1 = left
  // 2 = right 
  // 3 = backward
  // 4 = forward

struct VA* make(){
    struct VA* ant = (struct VA*)malloc(sizeof(struct VA)); 
	ant->itch = noitch;
	ant->row = 10;
	ant->col = 10;
	ant->energy = MAX_ENERGY;
	int i;
	int j;
	ant->m = malloc( sizeof(struct VA*)  * ant->row);
	for( i = 0; i < ant->row; i++ ){
        ant->m[i] = malloc(sizeof(struct VA*) * ant->col );
    }
	return ant;
}
struct Point MVF( struct Point element,struct VA*  ant ){	
	element.x++;
	ant->energy = 	ant->energy-3;
	return element;
}
struct Point MVB( struct Point element,struct VA*  ant){
	element.x--;
	ant->energy = 	ant->energy-3;
	return element;
}
struct Point MVL(struct Point element,struct VA*  ant){
	element.y--;
	ant->energy = 	ant->energy-3;
	return element;
}
struct Point MVR(struct Point element,struct VA*  ant){
	element.y++;
	ant->energy = 	ant->energy-3;
	return element;
}

int CWF(struct Point element,struct VA* ant){
	ant->energy = 	ant->energy-2;
	int tempx = element.x;
	int tempy = element.y;
	int r = ant->row;
    int count = 0;
    if(tempx+1 == r) {
        ant->itch = noitch;
        return 0;
    }
	else{
	    while((ant->m[tempy][tempx]!= '*')){
            count++;
            tempx++;
	    }
		ant->itch = forwarditch;
		return count;
	}
}
int CWB(struct Point element,struct VA* ant) {
    ant->energy = ant->energy - 2;
    int tempx = element.x;
    int tempy = element.y;
    int r = ant->row;
    int count = 0;
    if (tempx == 0) {
        ant->itch = noitch;
        return 0;
    } else {
        while ((ant->m[tempy][tempx] != '*')) {
            count++;
            tempx--;
        }
        ant->itch = backitch;
        return count;
    }
}
int CWL(struct Point element,struct VA* ant) {
    ant->energy = ant->energy - 2;
    int tempx = element.x;
    int tempy = element.y;
    int r = ant->row;
    int count = 0;
    if (tempy == 0) {
        ant->itch = noitch;
        return 0;
    } else {
        while ((ant->m[tempy][tempx] != '*')) {
            count++;
            tempy--;
        }
        ant->itch = leftitch;
        return count;
    }
}
int CWR(struct Point element,struct VA* ant){
    ant->energy = ant->energy - 2;
    int tempx = element.x;
    int tempy = element.y;
    int r = ant->row;
    int count = 0;
    if (tempy == r-1) {
        ant->itch = noitch;
        return 0;
    } else {
        while ((ant->m[tempy][tempx] != '*')) {
            count++;
            tempy++;
        }
        ant->itch = rightitch;
        return count;
    }
}
void BJPI(struct Point (*f)(struct Point,struct VA*),struct VA* ant,struct Point p){
		
		ant->energy = 	ant->energy-2;
}
void CJPI (struct Point (*f)(struct Point,struct VA*),struct VA* ant,struct Point p){
		ant->energy = 	ant->energy-2;
}
void RPnR(int n, int r,struct VA* ant, struct Point p){
		ant->energy = 	ant->energy-2;
		int i,j;
		for(i=0;i<r;i++){
			
		}
		
}



