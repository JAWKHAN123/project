#ifndef PROJECT_VA_H
#define PROJECT_VA_H
#include "point.h"

struct VA
{
    int itch;
  //  unsigned capacity;
    int row;
    int col;
    int energy;
	char** m;
//	Coordinate* array;
};
struct VA* make();
struct Point MVF(struct Point element,struct VA* ant);
struct Point MVB(struct Point element,struct VA* ant);
struct Point MVL(struct Point element,struct VA* ant);
struct Point MVR(struct Point element,struct VA* ant);
int CWF(struct Point element,struct VA* a);
int CWB(struct Point element,struct VA* a);
int CWL(struct Point element,struct VA* a);
int CWR(struct Point element,struct VA* a);
void BJPI(struct Point (*f)(struct Point,struct VA*),struct VA* ant,struct Point p);
void CJPI (struct Point (*f)(struct Point,struct VA*),struct VA* ant,struct Point p);
void RPnR(int n, int r,struct VA* ant,struct Point p);
#endif

