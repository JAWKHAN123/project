//
// Created by jawwa on 5/6/2020.
//
#include <stdio.h>
#include <stdlib.h>
#include "point.h"

struct stack{
    int max;
    int numElements;
    struct Point *arr;
};

struct stack* create(int max){
    struct stack* s = (struct stack*)malloc(sizeof(struct stack));
    s->numElements = 0;
    s->max = max;
    s->arr = (struct Point*)malloc(s->max * sizeof(struct Point));
    return s;
}
int isFull(struct stack* s){
    if(s->numElements == s->max)
        return 1;
    return 0;
}
int isEmpty(struct stack* s){
    if(s->numElements == 0)
        return 1;
    return 0;
}

void push(struct stack* s, struct Point element){
    if(isFull(s)){
        printf("stack is full");
        return;
    }
    s->arr[s->numElements] = element;
    s->numElements++;
}
struct Point pop(struct stack* s){
    if(isEmpty(s)){
        printf("stack is empty");
        exit(0);
    }
    s->numElements--;
    return s->arr[s->numElements];
}
struct Point peek(struct stack* s){
    if(isEmpty(s)){
        printf("stack is empty");
        exit(0);
    }
    //s->numElements--;
    return s->arr[s->numElements-1];
}

