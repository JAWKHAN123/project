#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "stack.h"
#include "point.h"
#include "va.h"

#define MAX_SIZE 20

int main() {
    struct stack *s = create(MAX_SIZE);

    FILE *input;
    FILE *output;
    FILE *instructions;

    input = fopen("in1.txt", "r");
    if (input == NULL) {
        printf("incorrect filename");
        return 0;
    }
    output = fopen("out1.txt", "w");
    if (output == NULL) {
        printf("incorrect filename");
        return 0;
    }
    instructions = fopen("instructions.txt", "w");
    if (instructions == NULL) {
        printf("incorrect filename");
        return 0;
    }

    char maze[10][10];
    char buf[5];
    int i = 0;
    int j = 0;
    char c;
    int x, y;

    fscanf(input, "%d", &x);
    fscanf(input, "%d", &y);
    fgetc(input);
    struct VA *ant = make();
    for ( i=0; i<10; i++){
        for ( j=0; j<10; j++){
            c=fgetc(input);

            maze[i][j]=c;
            if(c != '*'){
                maze[i][j] = ' ';
            }
            ant->m[i][j] = maze[i][j];
        }
        fgetc(input);//remove '/n'
    }

    for ( i=0; i<10; i++){
        for ( j=0; j<10; j++){
            printf("%c",ant->m[i][j]);
        }
        printf("\n");
    }

    printf("energy : %d \n", ant->energy);

    struct Point p1 = {x, y};
    struct Point p2;
    int count = 0;
    while(ant->energy > 0 || !feof(instructions)){
        fscanf(instructions, "%s", buf);
        if(strcmp(buf,"MVF") == 0){
            p1 = MVF(p1,ant);
        }
        else if(strcmp(buf,"MVB") == 0){
            p1 = MVB(p1,ant);
        }
        else if(strcmp(buf,"MVL") == 0){
            p1 = MVL(p1,ant);
        }
        else if(strcmp(buf,"MVR") == 0){
            p1 = MVR(p1,ant);
        }
        else if(strcmp(buf,"CWF") == 0){
            count = CWF(p1,ant);
        }
        else if(strcmp(buf,"CWB") == 0){
            count = CWB(p1,ant);
        }
        else if(strcmp(buf,"CWL") == 0){
            count = CWL(p1,ant);
        }
        else if(strcmp(buf,"CWR") == 0){
            count = CWR(p1,ant);
        }
        else if(strcmp(buf,"BJPI") == 0){

        }
        else if(strcmp(buf,"CJPI") == 0){

        }
        else if(strcmp(buf,"PUSH") == 0){
            push(s,p1);
            ant->energy-=4;
        }
        else if(strcmp(buf,"POP") == 0){
            p2 = pop(s);
            ant->energy-=4;
        }
        else if(strcmp(buf,"PEEK") == 0){
            p2 = peek(s);
            ant->energy-=4;
        }
        else if(strcmp(buf,"CLEAR") == 0){
            s = create(MAX_SIZE);
        }

    }


/*
    printf("(%d,%d)\n", p1.x, p1.y);
    int itch = CWF(p1,ant);
    printf("itch: %d ", itch);
    //printf("\n ant itch%s", ant->itch);
    p1 = MVF(p1, ant);
    //printf(" energy : %d \n", ant->energy);
    printf("\n(%d,%d)", p1.x, p1.y);
*/


}


